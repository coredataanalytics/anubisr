import csv
import subprocess
import sys


# def install(package):
#     subprocess.check_call([sys.executable, "-m", "pip", "install", package])
#
#
# install('pandas')
# install('parsel')
# install('selenium')
# install('webdriver_manager')
# install('pyinstaller')

from parsel import Selector
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager
import pandas as pd
from random import randint
import datetime
import math
from itertools import compress

import queue as Queue
from threading import Thread

FPA_list = pd.read_csv("data/FPA_url_list.csv", index_col=False)

def main_run_batch(arg):
    start = arg[0]
    stop = arg[1]
    chrome_options = webdriver.ChromeOptions();
    chrome_options.add_experimental_option("excludeSwitches", ['enable-automation']);
    driver = webdriver.Chrome('/Users/feiwang/Downloads/chromedriver', options=chrome_options)
    # driver = webdriver.Chrome('chromedriver.exe', options=chrome_options)
    FPA_list = pd.read_csv("data/FPA_url_list.csv", index_col=False)
    FPA_list["FPA_content"] = ""
    FPA_list["Name"] = ""
    FPA_list["Company"] = ""
    FPA_list["Mobile"] = ""
    FPA_list["Phone"] = ""
    FPA_list["Email"] = ""
    FPA_list["Website"] = ""
    FPA_list["Address_1"] = ""
    FPA_list["Address_2"] = ""
    FPA_list["Address_3"] = ""
    FPA_list["Address_4"] = ""

    FPA_list = FPA_list[start:stop]
    FPA_list = FPA_list.reset_index()

    for i in range(0,FPA_list.shape[0]):
        print(i)
        driver.get(FPA_list["url"][i])
        driver.get("http://www.google.com/")
        #driver.get("https://fpa.com.au/find-a-planner/bradley-speering-cfp321415/")
        sleep(1)

        repeat = True
        while repeat:
            sleep(0.5)
            try:
                contents = driver.find_element_by_id('member-details').text.split("\n")
            except:
                repeat = False
            if len(contents) > 0:
                repeat = False
                if list(compress(contents, ["Name" in i for i in contents]))[0].replace('Name', '').split(',')[0].lower().strip().split(" ")[0] not in FPA_list["url"][i]:
                    repeat = True

        FPA_list["FPA_content"][i] = contents
        if sum(["Name" in i for i in contents])>0:
            FPA_list["Name"][i]= list(compress(contents, ["Name" in i for i in contents]))[0].replace('Name','')
        if sum(["Company" in i for i in contents]) > 0:
            FPA_list["Company"][i]= list(compress(contents, ["Company" in i for i in contents]))[0].replace('Company','')
        if sum(["Mobile" in i for i in contents])>0:
            FPA_list["Mobile"][i]= list(compress(contents, ["Mobile" in i for i in contents]))[0].replace('Mobile','')
        if sum(["Phone" in i for i in contents])>0:
            FPA_list["Phone"][i]= list(compress(contents, ["Phone" in i for i in contents]))[0].replace('Phone','')
        if sum(["Email" in i for i in contents])>0:
            FPA_list["Email"][i]= list(compress(contents, ["Email" in i for i in contents]))[0].replace('Email','')
        if sum(["Website" in i for i in contents])>0:
            FPA_list["Website"][i]= list(compress(contents, ["Website" in i for i in contents]))[0].replace('Website','')
        address = list(compress(contents, ["Email" not in i for i in contents]))
        address = list(compress(address, ["Name" not in i for i in address]))
        address = list(compress(address, ["Company" not in i for i in address]))
        address = list(compress(address, ["Mobile" not in i for i in address]))
        address = list(compress(address, ["Phone" not in i for i in address]))
        address = list(compress(address, ["Website" not in i for i in address]))
        if len(address)>=1:
            FPA_list["Address_1"][i] = address[len(address)-1].replace('Address','')
        if len(address)>=2:
            FPA_list["Address_2"][i] = address[len(address)-2].replace('Address','')
        if len(address)>=3:
            FPA_list["Address_3"][i] = address[len(address)-3].replace('Address','')
        if len(address)>=4:
            FPA_list["Address_4"][i] = address[len(address)-4].replace('Address','')

    driver.close()
    return FPA_list


threads_list = list()
que = Queue.Queue()

t1 = Thread(target=lambda q, arg1: q.put(main_run_batch(arg1)),args=(que, [0, 30]))
t1.start()
threads_list.append(t1)
t2 = Thread(target=lambda q, arg1: q.put(main_run_batch(arg1)),args=(que, [30, 60]))
t2.start()
threads_list.append(t2)
t3 = Thread(target=lambda q, arg1: q.put(main_run_batch(arg1)),args=(que, [60, 90]))
t3.start()
threads_list.append(t3)



for t in threads_list:
    t.join()

content_list = pd.DataFrame()
while not que.empty():
    result = que.get()
    content_list = content_list.append(result)

content_list.sort_index().to_csv("data/FPA_url_list_finished"  + ".csv")