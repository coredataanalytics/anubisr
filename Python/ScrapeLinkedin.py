import csv
import subprocess
import sys
import mysql.connector
from mysql.connector import Error
# def install(package):
#     subprocess.check_call([sys.executable, "-m", "pip", "install", package])
# install('pymysql')
# install('parsel')
# install('selenium')
# install('webdriver_manager')
# install('pyinstaller')
# install('mysql-connector-python')

from parsel import Selector
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager
import pandas as pd
from random import randint
import datetime
import math
from sqlalchemy import create_engine
import pymysql



chrome_options = webdriver.ChromeOptions();
chrome_options.add_experimental_option("excludeSwitches", ['enable-automation']);
# driver = webdriver.Chrome('chromedriver.exe',options=chrome_options)
driver = webdriver.Chrome('/Users/feiwang/Downloads/chromedriver',options=chrome_options)



def getLinkedinProfile(url):
    if 'linkedin.com' in url:
        driver.get(url)
        sleep(1)

        sel = Selector(text=driver.page_source)

        name = sel.xpath('//*[@class = "inline t-24 t-black t-normal break-words"]/text()')
        if len(name) != 0:
            name = name.extract_first().split()
            name = ' '.join(name)

        position = sel.xpath('//*[@class = "mt1 t-18 t-black t-normal"]/text()')
        if len(position) != 0:
            position = position.extract_first().strip()

        education = ""
        company = ""

        company_education = sel.xpath(
            '//*[@class = "text-align-left ml2 t-14 t-black t-bold full-width lt-line-clamp lt-line-clamp--multi-line ember-view"]/text()')
        if len(company_education) != 0:

            company = company_education[0].root.strip()
            if len(company_education) > 1:
                education = company_education[1].root.strip()

        location = ""
        linkedin_connections = ""
        location = sel.xpath('//*[@ class = "t-16 t-black t-normal inline-block"]/text()')
        if len(location) != 0:
            location = ' '.join(location.extract_first().split())

        linkedin_connections = sel.xpath('//*[@class = "t-16 t-black t-normal"]/text()')
        if len(linkedin_connections) != 0:
            linkedin_connections = ' '.join(linkedin_connections.extract_first().split())

        positions_text = ""
        companies_text = ""
        durations_text = ""
        date_ranges_text = ""

        positions = sel.xpath('//*[@class = "t-16 t-black t-bold"]/text()')
        positions_ = []
        for x in positions:
            if x.root.strip() != "":
                positions_ = positions_ + [x.root.strip()]
        positions_hidden = sel.xpath("//span[.='Title']/following-sibling::span")
        companies = sel.xpath('//*[@class = "pv-entity__secondary-title t-14 t-black t-normal"]/text()')
        companies_hidden = sel.xpath("//span[.='Company Name']/following-sibling::span")
        durations = sel.xpath('//*[@class = "pv-entity__bullet-item-v2"]/text()')
        date_ranges = sel.xpath("//span[.='Dates Employed']/following-sibling::span")

        positions_ = positions_[0:len(companies)]

        if (len(positions_) == len(companies) == len(date_ranges)):
            for i in range(0, len(positions_)):
                positions_text = positions_text + " || " + positions_[i]
                companies_text = companies_text + " || " + companies[i].root.strip()
                durations_text = durations_text + " || " + durations[i].root.strip()
                date_ranges_text = date_ranges_text + " || " + date_ranges[i].root.text.strip()

        # this to to parse the contact info

        twitter = ""
        website = ""
        email = ""
        driver.get(url.replace('au.', "www.", 1) + "/detail/contact-info/")
        sel = Selector(text=driver.page_source)
        Contact_names = sel.xpath('//*[@class = "pv-contact-info__header t-16 t-black t-bold"]/text()')
        Contact_links = sel.xpath('//*[@class = "pv-contact-info__contact-link t-14 t-black t-normal"]/text()')

        for i in range(0, len(Contact_names)):
            if (Contact_names[i].root.strip() == "Twitter"):
                twitter = Contact_links[i].root.strip()
            if (Contact_names[i].root.strip() == "Website"):
                website = Contact_links[i].root.strip()
            if (Contact_names[i].root.strip() == "Email"):
                email = Contact_links[i].root.strip()

        print('\n')
        print('Name: ', name)
        print('Position: ', position)
        print('Positinos_history: ', positions_text)
        print('Companies_history: ', companies_text)
        print('Date_ranges_history: ', date_ranges_text)
        print('Company: ', company)
        print('Education: ', education)
        print('Location: ', location)
        print('Connections: ', linkedin_connections)
        print('Twitter: ', twitter)
        print('Website: ', website)
        print('Email: ', email)
        print('URL: ', url)
        print('\n')
        return [name,position,company,education,location,linkedin_connections,positions_text,companies_text,date_ranges_text,twitter,website,email,url]




def getLinkedinProfileNologin(url):
    if 'linkedin.com' in url:

        # url = financial_planner_unique['linkedin'][3]


        driver.get(url)
        sleep(1)

        sel = Selector(text=driver.page_source)

        name = sel.xpath('//*[@class = "top-card-layout__title"]/text()')
        # name = sel.xpath('//*[@class = "inline t-24 t-black t-normal break-words"]/text()')
        if len(name) != 0:
            name = name.extract_first().split()
            name = ' '.join(name)

        position = sel.xpath('//*[@class = "top-card-layout__headline"]/text()')
        # position = sel.xpath('//*[@class = "mt1 t-18 t-black t-normal"]/text()')
        if len(position) != 0:
            position = position.extract_first().strip()

        education = ""
        company = ""

        company_education = sel.xpath('//*[@class = "top-card-link__description"]/text()')
        # company_education = sel.xpath('//*[@class = "text-align-left ml2 t-14 t-black t-bold full-width lt-line-clamp lt-line-clamp--multi-line ember-view"]/text()')
        if len(company_education) != 0:

            company = company_education[0].root.strip()
            if len(company_education) > 1:
                education = company_education[1].root.strip()

        location = ""
        linkedin_connections = ""
        location = sel.xpath('//*[@ class = "top-card__subline-item"]/text()')
        # location = sel.xpath('//*[@ class = "t-16 t-black t-normal inline-block"]/text()')

        if len(location) != 0:
            location = ' '.join(location.extract_first().split())

        linkedin_connections = sel.xpath('//*[@class = "top-card__subline-item top-card__subline-item--bullet"]/text()')
        # linkedin_connections = sel.xpath('//*[@class = "t-16 t-black t-normal"]/text()')
        if len(linkedin_connections) != 0:
            linkedin_connections = ' '.join(linkedin_connections.extract_first().split())

        positions_text = ""
        companies_text = ""
        durations_text = ""
        date_ranges_text = ""
        #
        # positions = sel.xpath('//*[@class = "t-16 t-black t-bold"]/text()')
        # positions_ = []
        # for x in positions:
        #     if x.root.strip() != "":
        #         positions_ = positions_ + [x.root.strip()]
        # positions_hidden = sel.xpath("//span[.='Title']/following-sibling::span")
        # companies = sel.xpath('//*[@class = "pv-entity__secondary-title t-14 t-black t-normal"]/text()')
        # companies_hidden = sel.xpath("//span[.='Company Name']/following-sibling::span")
        # durations = sel.xpath('//*[@class = "pv-entity__bullet-item-v2"]/text()')
        # date_ranges = sel.xpath("//span[.='Dates Employed']/following-sibling::span")
        #
        # positions_ = positions_[0:len(companies)]
        #
        # if (len(positions_) == len(companies) == len(date_ranges)):
        #     for i in range(0, len(positions_)):
        #         positions_text = positions_text + " || " + positions_[i]
        #         companies_text = companies_text + " || " + companies[i].root.strip()
        #         durations_text = durations_text + " || " + durations[i].root.strip()
        #         date_ranges_text = date_ranges_text + " || " + date_ranges[i].root.text.strip()
        #
        # # this to to parse the contact info
        #
        twitter = ""
        website = ""
        email = ""
        # driver.get(url.replace('au.', "www.", 1) + "/detail/contact-info/")
        # sel = Selector(text=driver.page_source)
        # Contact_names = sel.xpath('//*[@class = "pv-contact-info__header t-16 t-black t-bold"]/text()')
        # Contact_links = sel.xpath('//*[@class = "pv-contact-info__contact-link t-14 t-black t-normal"]/text()')
        #
        # for i in range(0, len(Contact_names)):
        #     if (Contact_names[i].root.strip() == "Twitter"):
        #         twitter = Contact_links[i].root.strip()
        #     if (Contact_names[i].root.strip() == "Website"):
        #         website = Contact_links[i].root.strip()
        #     if (Contact_names[i].root.strip() == "Email"):
        #         email = Contact_links[i].root.strip()

        print('\n')
        print('Name: ', name)
        print('Position: ', position)
        print('Positinos_history: ', positions_text)
        print('Companies_history: ', companies_text)
        print('Date_ranges_history: ', date_ranges_text)
        print('Company: ', company)
        print('Education: ', education)
        print('Location: ', location)
        print('Connections: ', linkedin_connections)
        print('Twitter: ', twitter)
        print('Website: ', website)
        print('Email: ', email)
        print('URL: ', url)
        print('\n')
        return [name,position,company,education,location,linkedin_connections,positions_text,companies_text,date_ranges_text,twitter,website,email,url]



driver.get('https://www.linkedin.com/')
sleep(0.5)
#
username = driver.find_element_by_name("session_key")
username.send_keys('asdfasdfasasdf@gmail.com')
sleep(0.5)

password = driver.find_element_by_name('session_password')
password.send_keys('asdfasdfasdfasdf')
sleep(0.5)


sign_in_button = driver.find_element_by_class_name('sign-in-form__submit-btn')
sign_in_button.click()
sleep(2)




connection = mysql.connector.connect(host='103.27.33.160',database='anubisda_advisers', user='anubisda_admin', password='ct3DqVc@8w5w*N')

scrape_linkedin_urls = pd.read_sql("select distinct linkedin_url "
                                   "from scrape_linkedin_urls "
                                   "where linkedin_url <> 'None' "
                                   "and linkedin_url not in "
                                   "(select linkedin_url from scrape_linkedin_contents)  limit 200", connection)


scrape_linkedin_urls['name']=''
scrape_linkedin_urls['position']=''
scrape_linkedin_urls['company']=''
scrape_linkedin_urls['education']=''
scrape_linkedin_urls['location']=''
scrape_linkedin_urls['linkedin_connections']=''
scrape_linkedin_urls['positions_text']=''
scrape_linkedin_urls['companies_text']=''
scrape_linkedin_urls['date_ranges_text']=''
scrape_linkedin_urls['twitter']=''
scrape_linkedin_urls['website']=''
scrape_linkedin_urls['email']=''
scrape_linkedin_urls['url']=''


for i in range(0,100):
    sleep(10)
    if(str(scrape_linkedin_urls["linkedin_url"][i])!="nan"):
        if ('linkedin.com' in str(scrape_linkedin_urls["linkedin_url"][i]) ) and ('google.com' not in str(scrape_linkedin_urls["linkedin_url"][i])) :
            linkedinProfile=getLinkedinProfile(str(scrape_linkedin_urls["linkedin_url"][i]))
            if (len(linkedinProfile[0]))>0:
                scrape_linkedin_urls['name'][i]=linkedinProfile[0]
                scrape_linkedin_urls['position'][i]=linkedinProfile[1]
                scrape_linkedin_urls['company'][i]=linkedinProfile[2]
                scrape_linkedin_urls['education'][i]=linkedinProfile[3]
                scrape_linkedin_urls['location'][i]=linkedinProfile[4]
                scrape_linkedin_urls['linkedin_connections'][i]=linkedinProfile[5]
                scrape_linkedin_urls['positions_text'][i]=linkedinProfile[6]
                scrape_linkedin_urls['companies_text'][i]=linkedinProfile[7]
                scrape_linkedin_urls['date_ranges_text'][i]=linkedinProfile[8]
                scrape_linkedin_urls['twitter'][i]=linkedinProfile[9]
                scrape_linkedin_urls['website'][i]=linkedinProfile[10]
                scrape_linkedin_urls['email'][i]=linkedinProfile[11]
                scrape_linkedin_urls['url'][i]=linkedinProfile[12]
    print(i)


scrape_linkedin_urls[0:24].to_csv("data/scrape_linkedin_contents.csv")
