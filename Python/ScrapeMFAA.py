
import csv
import subprocess
import sys
import argparse
import pickle
import numpy as np
from parsel import Selector
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import pandas as pd
from random import randint
import datetime
import math
import requests
from bs4 import BeautifulSoup
# from webdriver_manager.chrome import ChromeDriverManager
import shutil
import multiprocessing

# dir_photos = 'E:/Photos/StoryPark'
options = webdriver.ChromeOptions()

driver = webdriver.Chrome('chromedriver.exe', options=options)
# driver = webdriver.Chrome(ChromeDriverManager().install())

driver.get('https://www.mortgageandfinancehelp.com.au/find-accredited-broker/')


for i in range(200):
    print(i)
    form = driver.find_elements_by_link_text('Load more brokers')
    form[0].click()
    sleep(1)



soup = BeautifulSoup(driver.page_source, 'html.parser')
notifications_soup = soup.find_all('a', attrs={'id': 'reveal_fullprofile'})

result_set = pd.DataFrame(
    columns=['external_id','preferred_name', 'last_name', 'phone', 'mobile', 'email', 'city', 'state', 'company'])
for i in range(len(notifications_soup)):
    mobile = ''
    company= ''
    phone= ''
    city= ''
    try:
        mobile = notifications_soup[i].attrs['data-mobile']
    except:
        pass
    try:
        company = notifications_soup[i].attrs['data-company']
    except:
        pass
    try:
        phone = notifications_soup[i].attrs['data-phone']
    except:
        pass
    try:
        city = notifications_soup[i].attrs['data-city']
    except:
        pass

    result_set.loc[i] = [ notifications_soup[i].attrs['data-external_id']
        , notifications_soup[i].attrs['data-preferred_name']
        , notifications_soup[i].attrs['data-last_name']
        , phone
        , mobile
        , notifications_soup[i].attrs['data-email']
        , city
        , notifications_soup[i].attrs['data-state']
        , company
                         ]


result_set.to_csv('data/MFAA/mfaa.csv')

