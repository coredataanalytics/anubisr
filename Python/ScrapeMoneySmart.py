import csv
import subprocess
import sys


# def install(package):
#     subprocess.check_call([sys.executable, "-m", "pip", "install", package])
#
#
# install('queuelib')
# install('parsel')
# install('selenium')
# install('webdriver_manager')
# install('pyinstaller')

from parsel import Selector
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
# from webdriver_manager.chrome import ChromeDriverManager
import pandas as pd
from random import randint
import datetime
import math

import queue as Queue

from threading import Thread
from webdriver_manager.chrome import ChromeDriverManager



def getFPAeach(ASICid,driver):


    # ASICid=str(financial_planner_unique.ADV_NUMBER[i])
    sleep(1)
    driver.get('https://www.moneysmart.gov.au/investing/financial-advice/financial-advisers-register')
    sleep(1)
    inputElement = driver.find_element_by_id("searchDetails")
    inputElement.clear()
    inputElement.send_keys(ASICid)
    sleep(1)
    try:
        driver.find_element_by_xpath("//button[@class='search']").click()
    except:
        driver.find_element_by_xpath("//button[@class='search']").click()

    repeat = True
    element=""
    p=1
    while repeat:
        element = driver.find_elements_by_class_name('number')
        sleep(1)
        p = p +1
        # print(p)
        if len(element)>0 or p>20:
            repeat = False

    contents = ""
    qualifications = ""
    memberships = ""
    if len(element) > 0:
        sleep(1)
        try:
            element[0].click()
        except:
            element[0].click()

        sleep(1)
        repeat = True
        while repeat:
            sleep(1.5)
            try:
                contents = driver.find_element_by_xpath("//div[@class='break first']").text
                qualifications = driver.find_element_by_xpath("//div[@class='qualifications']").text
                memberships = driver.find_element_by_xpath("//div[@class='details-box membership']").text

            except:
                repeat = False
            if len(contents) > 0:
                repeat = False
    return ([p,contents,qualifications,memberships])


def getFPAeach_list(ranges):
    financial_planner_unique=ranges[2]
    chrome_options = webdriver.ChromeOptions();
    chrome_options.add_experimental_option("excludeSwitches", ['enable-automation']);
    # driver = webdriver.Chrome(ChromeDriverManager().install())
    driver = webdriver.Chrome('chromedriver.exe', options=chrome_options)
    print(ranges)
    result_df = pd.DataFrame()
    for i in range(ranges[0], ranges[1]):
        if i >= 0:
            print(i)
            content = ""
            repeat = True
            while repeat:
                try:
                    content = getFPAeach(str(financial_planner_unique.ADV_NUMBER[i]),driver)
                    repeat = False
                    # if content[0]==21:
                    #     repeat = True
                except:
                    repeat = True
            # print(content[1])
            financial_planner_unique["FAR_content"][i] = content[1]
            financial_planner_unique["qualifications"][i] = content[2]
            financial_planner_unique["memberships"][i] = content[3]
            # if ((i + 1) % 200 == 0) or ((i + 1)==financial_planner_unique.shape[0]):
                # financial_planner_unique[(i + 1 - 200):(i + 1)].to_csv( "data/finalcial_planners_simple_output_FAR_" + str(i + 1) + ".csv")
            result_df = result_df.append(financial_planner_unique[i:(i+1)])

    driver.close()
    return result_df


def main_run_batch(start,stop):

    financial_planner_unique = pd.read_csv("data/ASIC/FINANCIAL_ADVISERS_LATEST.csv", index_col=False, encoding = "ISO-8859-1", engine='python')
    #financial_planner_unique = financial_planner_unique[(financial_planner_unique['ADV_ROLE_STATUS'] == "Current")]
    financial_planner_unique = pd.DataFrame(financial_planner_unique, columns=['ADV_NUMBER', 'MEMBERSHIPS',"QUALIFICATIONS_AND_TRAINING"]).drop_duplicates()

    financial_planner_unique = financial_planner_unique.drop_duplicates()
    financial_planner_unique["FAR_content"] = ""
    financial_planner_unique["qualifications"] = ""
    financial_planner_unique["memberships"] = ""
    financial_planner_unique = financial_planner_unique[start:stop]
    financial_planner_unique = financial_planner_unique.reset_index()
    # stop = financial_planner_unique.shape[0]
    que = Queue.Queue()
    threads_list = list()
    bucket_size =2000
    bucket_num = int(math.ceil( financial_planner_unique.shape[0]/bucket_size))

    for i in range(0,bucket_num):
        range_1 = i * bucket_size
        range_2 = (i+1) * bucket_size
        if i== bucket_num-1:
            range_2= financial_planner_unique.shape[0]

        # t1 = Thread(target=getFPAeach_list, args=([range_1, range_2], financial_planner_unique))
        t1 = Thread(target=lambda q, arg1: q.put(getFPAeach_list(arg1)), args=(que, [range_1, range_2, financial_planner_unique]))

        t1.start()
        threads_list.append(t1)

    for t in threads_list:
        t.join()

    content_list= pd.DataFrame()
    while not que.empty():
        result=que.get()
        content_list= content_list.append(result)
    content_list.sort_index().to_csv( "data/MoneySmart/finalcial_planners_simple_output_FAR_"+str(stop)+".csv")
# #
main_run_batch(0,8000)
main_run_batch(8000,16000)
main_run_batch(16000,24000)
main_run_batch(24000,32000)
main_run_batch(32000,40000)

# with error result  1247960
# replacing with empty result 1235798
