

#Import ASIC data ---------------
financial_planner_dictionary  <- readxl::read_xlsx(paste0(root_path,"/data/ASIC/FINANCIAL_ADVISERS_DICTIONARY.xlsx"))
financial_planner_dictionary  <- financial_planner_dictionary[grepl('FIN_', (financial_planner_dictionary$Field)),]
financial_planner_dictionary$Field <- gsub('\r','',financial_planner_dictionary$Field )
financial_planner_dictionary$Field <- gsub('\n','',financial_planner_dictionary$Field )
financial_planner_dictionary$Description <- gsub('\r\n',' ',financial_planner_dictionary$Description )
financial_planner_dictionary$Description <- trimws(gsub('Provide Financial Product Advice -','', (financial_planner_dictionary$Description)))



financial_planner  <- read.csv(paste0(root_path,"/data/ASIC/FINANCIAL_ADVISERS_LATEST.csv"),1, stringsAsFactors = F)
financial_planner[is.na(financial_planner)] <- ""

financial_planner$ADV_START_DT <-  as.Date(as.POSIXct(financial_planner$ADV_START_DT, format = "%d/%m/%Y"))
financial_planner$ADV_END_DT   <-  as.Date(as.POSIXct(financial_planner$ADV_END_DT  , format = "%d/%m/%Y"))
financial_planner$ADV_END_DT[is.na(financial_planner$ADV_END_DT)] <- Sys.Date()



#  authorized_financial_services
financial_planner$list_of_authorized_financial_services <- ''
financial_planner$number_of_authorized_financial_services <- ''

service_names <- colnames(financial_planner)[grepl('FIN_', colnames(financial_planner))] 
services <- financial_planner[,service_names] 
services[services==''] <- '0'



data_long <- gather(cbind(financial_planner$ADV_NUMBER,services), service, value, 'FIN_CARBUNIT':'FIN_SECURALL', factor_key=TRUE)
data_long <- data_long[data_long$value==1 ,]


#long table

financial_planner$number_of_authorized_financial_services <- colSums(apply(services,1, as.numeric))

service_names <- financial_planner_dictionary[financial_planner_dictionary$Field %in% service_names ,c('Field','Description')]

data_long <- merge(data_long, service_names, by.x='service', by.y= 'Field')
# data_long <- select(data_long, member_id= 'financial_planner$ADV_NUMBER', product_name= 'Description')
data_long <- select(data_long, member_id= 'financial_planner$ADV_NUMBER', product_name= 'service')

# 1. FIN_SUPER = Superannuation
# 2. FIN_SUPER_SMSUPER = SMSF (Self-Managed Super Funds)
# 3. FIN_SECUR = Securities
# 4. FIN_LIFEPROD_LIFEINV, FIN_LIFEPROD_LIFERISK, FIN_LIFEPROD_LIFERISK_LIFECONS = Insurance
# 5. FIN_GOVDEB = Bonds
# 6. FIN_DERIV, FIN_DERIV_DERWOOL, FIN_DERIV_DERELEC, FIN_DERIV_DERGRAIN = Derivatives
# 7. FIN_DEPPAY_DPNONBAS, FIN_DEPPAY_DPNONCSH = Cash/Term Deposits
# 8. FIN_FOREXCH = Foreign Exchange Contracts

data_long <- data_long[data_long$product_name %in% c('FIN_SUPER'
                                                     ,'FIN_SUPER_SMSUPER'
                                                     ,'FIN_SECUR'
                                                     ,'FIN_LIFEPROD_LIFEINV', 'FIN_LIFEPROD_LIFERISK', 'FIN_LIFEPROD_LIFERISK_LIFECONS'
                                                     ,'FIN_GOVDEB'
                                                     ,'FIN_DERIV','FIN_DERIV_DERWOOL','FIN_DERIV_DERELEC', 'FIN_DERIV_DERGRAIN'
                                                     ,'FIN_DEPPAY_DPNONBAS','FIN_DEPPAY_DPNONCSH'
                                                     ,'FIN_FOREXCH'
) ,]

data_long$product_name <- as.character(data_long$product_name)

data_long$product_name[data_long$product_name==      'FIN_SUPER'] <- 'Superannuation'
data_long$product_name[data_long$product_name==      'FIN_SUPER_SMSUPER'] <- 'SMSF (Self-Managed Super Funds)'
data_long$product_name[data_long$product_name==      'FIN_SECUR'] <- 'Securities'
data_long$product_name[data_long$product_name %in% c('FIN_LIFEPROD_LIFEINV','FIN_LIFEPROD_LIFERISK','FIN_LIFEPROD_LIFERISK_LIFECONS')] <- 'Insurance'
data_long$product_name[data_long$product_name==      'FIN_GOVDEB'] <- 'Bonds'
data_long$product_name[data_long$product_name %in% c('FIN_DERIV','FIN_DERIV_DERWOOL','FIN_DERIV_DERELEC','FIN_DERIV_DERGRAIN')] <- 'Derivatives'
data_long$product_name[data_long$product_name %in% c('FIN_DEPPAY_DPNONBAS','FIN_DEPPAY_DPNONCSH')] <- 'Cash/Term Deposits'
data_long$product_name[data_long$product_name==      'FIN_FOREXCH'] <- 'Foreign Exchange Contracts'


data_long <- unique(data_long)

data_long$filter_name <- as.factor(data_long$product_name)
data_long$filter_id   <- as.numeric(data_long$filter_name)
data_long$filter_name <- as.character(data_long$filter_name)


member_filter_product_type <- data_long
member_filter_product_type$member_id <- paste0('01_',member_filter_product_type$member_id)
member_filter_product_type$id <-  1:nrow(member_filter_product_type)
filter_product_type <- unique(select(member_filter_product_type, filter_id, filter_name ))




for(i in 1:nrow(service_names))
{
  colnames(services)[colnames(services)==service_names$Field[i]] <- service_names$Description[i]
}


for(i in 1:nrow(service_names))
{
  services[, i][services[, i]=='1'] <-  colnames(services)[i]
}
services[services=='0'] <- NA

financial_planner$list_of_authorized_financial_services <- apply(services, 1, paste, collapse="|")
financial_planner$list_of_authorized_financial_services <- gsub('NA\\|','', financial_planner$list_of_authorized_financial_services )
financial_planner$list_of_authorized_financial_services <- gsub('NA','', financial_planner$list_of_authorized_financial_services )


financial_planner <- financial_planner[,colnames(financial_planner)[!grepl('FIN_', colnames(financial_planner))]] 








#Members ----
financial_planner <- setDT(financial_planner)[,licence_size:=n_distinct(ADV_NUMBER[ADV_ROLE_STATUS=='Current']),by= "LICENCE_NUMBER"]

financial_planner_unique <- setDT(financial_planner)[,list( 
    member_status=   ifelse(sum((ADV_ROLE_STATUS)=="Current")>0,"Current","Ceased")
  , ADV_ADD_PCODE= ADV_ADD_PCODE[which.max(ADV_START_DT)]
  , ADV_ADD_LOCAL= ADV_ADD_LOCAL[which.max(ADV_START_DT)]
  , ADV_ADD_STATE= ADV_ADD_STATE[which.max(ADV_START_DT)]
  , ADV_ADD_COUNTRY= ADV_ADD_COUNTRY[which.max(ADV_START_DT)]
  , first_advice= as.numeric(ADV_FIRST_PROVIDED_ADVICE[which.max(ADV_START_DT)])
  , year_covered_with_licence = round(as.numeric(sum(ADV_END_DT- ADV_START_DT)/365),2)
  , licence_id    = LICENCE_NUMBER[ADV_END_DT==ADV_END_DT[which.max(ADV_END_DT)]] [which.max(licence_size[ADV_END_DT==ADV_END_DT[which.max(ADV_END_DT)]])]
  , licence_name  = tolower(LICENCE_NAME  [ADV_END_DT==ADV_END_DT[which.max(ADV_END_DT)]] [which.max(licence_size[ADV_END_DT==ADV_END_DT[which.max(ADV_END_DT)]])])
  , list_of_authorized_financial_services  = list_of_authorized_financial_services[ADV_END_DT==ADV_END_DT[which.max(ADV_END_DT)]] [which.max(licence_size[ADV_END_DT==ADV_END_DT[which.max(ADV_END_DT)]])]
  , number_of_authorized_financial_services  = number_of_authorized_financial_services[ADV_END_DT==ADV_END_DT[which.max(ADV_END_DT)]] [which.max(licence_size[ADV_END_DT==ADV_END_DT[which.max(ADV_END_DT)]])]
  # , licence_name = LICENCE_NAME  [ADV_END_DT==ADV_END_DT[which.max(ADV_END_DT)] & licence_size==licence_size[which.max(licence_size)] ]
  , licence_id_previous   = LICENCE_NUMBER[ADV_ROLE_STATUS=='Ceased'][which.max(ADV_START_DT[ADV_ROLE_STATUS=='Ceased'])]
  , licence_name_previous = LICENCE_NAME[ADV_ROLE_STATUS=='Ceased'][which.max(ADV_START_DT[ADV_ROLE_STATUS=='Ceased'])]
  , is_self_licensed= licence_size[ADV_ROLE_STATUS=='Current'][which.max(licence_size[ADV_ROLE_STATUS=='Current'])]==1
)
  ,by=c('ADV_NAME','ADV_NUMBER','QUALIFICATIONS_AND_TRAINING','MEMBERSHIPS')]


financial_planner_unique[is.na(financial_planner_unique)] <- ""

financial_planner_unique$licence_id_previous[!financial_planner_unique$member_status=='Current'] <- ''
financial_planner_unique$licence_name_previous[!financial_planner_unique$member_status=='Current'] <- ''
# financial_planner_unique$year_of_experience <- year(Sys.Date()) - financial_planner_unique$first_advice

# financial_planner_unique <- financial_planner_unique[!financial_planner_unique$member_status=="Current",]








# financial_planner_unique[financial_planner_unique==""] <- NA
financial_planner_unique$ADV_NAME <- tolower(financial_planner_unique$ADV_NAME)

financial_planner_unique <- cbind(financial_planner_unique,First_last_from_full_name(tolower(financial_planner_unique$ADV_NAME)) )


financial_planner_unique$linkedin_url_tried <- ""
financial_planner_unique$linkedin_url <- ""


colnames(financial_planner_unique)[colnames(financial_planner_unique)=="ADV_NAME"] <- "full_name"
colnames(financial_planner_unique)[colnames(financial_planner_unique)=="ADV_NUMBER"] <- "asic_id"
colnames(financial_planner_unique)[colnames(financial_planner_unique)=="QUALIFICATIONS_AND_TRAINING"] <- "qualifications"
colnames(financial_planner_unique)[colnames(financial_planner_unique)=="ADV_ADD_PCODE"] <- "address_postcode"
colnames(financial_planner_unique)[colnames(financial_planner_unique)=="ADV_ADD_STATE"] <- "address_state"
colnames(financial_planner_unique)[colnames(financial_planner_unique)=="ADV_ADD_LOCAL"] <- "address_city"
colnames(financial_planner_unique)[colnames(financial_planner_unique)=="MEMBERSHIPS"] <- "memberships"
financial_planner_unique$address_street <- ""
financial_planner_unique$address_unit <- ""


members <- select(financial_planner_unique,"asic_id","full_name","first_name","last_name","first_advice","year_covered_with_licence","memberships","qualifications",'is_self_licensed', 'licence_id','licence_name','member_status', 'list_of_authorized_financial_services','number_of_authorized_financial_services','licence_id_previous','licence_name_previous'
                  ,"address_state","address_city","address_postcode","address_street","address_unit")


members$address_city_source<- ""
members$address_state_source <- ""
members$address_postcode_source <- ""
members$address_street_source <- ""
members$address_unit_source <- ""

members$address_city_source[members$address_city!=""]<- "ASIC"
members$address_state_source[members$address_state!=""] <- "ASIC"
members$address_postcode_source[members$address_postcode!=""] <- "ASIC"
members$address_street_source[members$address_street!=""] <- "ASIC"
members$address_unit_source[members$address_unit!=""] <- "ASIC"


members$dob_year <- ""
members$gender <- ""
members$email <- ""
members$mobile_phone <- ""
members$work_phone <- ""
members$company <- ""
members$job_title <- ""
members$website <- ""
members$linkedin_url <- ""
members$advice_style <- ""
members$is_risk_adviser <- ""


members$gender_source <- ""
members$email_source <- ""
members$mobile_phone_source <- ""
members$work_phone_source <- ""
members$company_source <- ""
members$job_title_source <- ""
members$website_source <- ""
members$linkedin_url_source <- ""
members$memberships_source <- ""
members$qualifications_source <- ""
members$dob_year_source  <- ""



member_fields <- colnames(members)
  #Import Money Smart   ----
print(" import Money Smart  ")

scrape_money_smart  <- read.csv(paste0(root_path,"/data/MoneySmart/MoneySmart.csv"),stringsAsFactors = F)
scrape_money_smart <- select (scrape_money_smart, asic_id= "ADV_NUMBER", "qualifications","memberships", "address_state", "address_city","address_postcode","address_street","address_unit")
# DB_write_table(scrape_money_smart        ,"anubisda_advisers","scrape_money_smart")

members <- merge(members,scrape_money_smart , by= "asic_id",all.x=T)
members[is.na(members)] <- ""


members$address_city<- members$address_city.x
members$address_state <- members$address_state.x
members$address_postcode <- members$address_postcode.x
members$address_street <- members$address_street.x
members$address_unit <- members$address_unit.x
members$memberships    <- members$memberships.x
members$qualifications <- members$qualifications.x

members$address_city_source[!members$address_city.x=="" ] <- "ASIC"
members$address_state_source[!members$address_state.x=="" ] <- "ASIC"
members$address_postcode_source[!members$address_postcode.x=="" ] <- "ASIC"
members$address_street_source[!members$address_street.x=="" ] <- "ASIC"
members$address_unit_source[!members$address_unit.x=="" ]  <- "ASIC"
members$memberships_source[!members$memberships.x=="" ]  <- "ASIC"
members$qualifications_source[!members$qualifications.x=="" ]  <- "ASIC"


members$address_city[!members$address_city.y=="" ]         <- members$address_city.y[!members$address_city.y=="" ]
members$address_state[!members$address_state.y=="" ]    <- members$address_state.y[!members$address_state.y=="" ]
members$address_postcode[!members$address_postcode.y=="" ] <- members$address_postcode.y[!members$address_postcode.y=="" ]
members$address_street [!members$address_street.y=="" ]    <- members$address_street.y[!members$address_street.y=="" ]
members$address_unit[!members$address_unit.y=="" ]         <- members$address_unit.y[!members$address_unit.y=="" ]

members$memberships[!members$memberships.y=="" ]         <- members$memberships.y[!members$memberships.y=="" ]
members$qualifications[!members$qualifications.y=="" ]         <- members$qualifications.y[!members$qualifications.y=="" ]


members$address_city_source[!members$address_city.y=="" ]      <- "MoneySmart"
members$address_state_source[!members$address_state.y=="" ]    <- "MoneySmart"
members$address_postcode_source[!members$address_postcode.y=="" ] <- "MoneySmart"
members$address_street_source[!members$address_street.y=="" ]  <- "MoneySmart"
members$address_unit_source[!members$address_unit.y=="" ]      <- "MoneySmart"
members$memberships_source[!members$memberships.y=="" ]        <- "MoneySmart"
members$qualifications_source[!members$qualifications.y=="" ]  <- "MoneySmart"


members <- setDF(members)[,member_fields]

# DB_write_table(members        ,"anubisda_advisers","members")
#Member licence ----

member_licences <- unique(select(financial_planner,asic_id= "ADV_NUMBER",start_date="ADV_START_DT",end_date="ADV_END_DT", status="ADV_ROLE_STATUS",licence_id= "LICENCE_NUMBER", licence_name= "LICENCE_NAME",licence_abn= "LICENCE_ABN",licence_controlled_by= "LICENCE_CONTROLLED_BY"))

# DB_write_table(member_licences,"anubisda_advisers","member_licences")


# Lime Survey ----
if(FALSE)
{
  lime_survey <- read.csv(paste0(root_path,"/data/LimeSurvey/lime_survey.csv"),stringsAsFactors = FALSE)
  lime_survey$Date.submitted <- as.Date(lime_survey$Date.submitted)
  
  
  lime_how_old <- lime_survey[lime_survey$surey_question=='How.old.are.you.'  ,]
  lime_how_old$dob_year <- as.integer(substring(lime_how_old$Date.submitted,1,4)) - as.integer( lime_how_old$surey_answer)
  lime_how_old <- lime_how_old[!is.na(lime_how_old$dob_year),]
  lime_how_old <- setDT(lime_how_old)[,  list( dob_year = as.numeric(dob_year[which.max(Date.submitted)]) )
                                      ,by='asic_id']
  
  write.csv(lime_how_old,paste0(root_path,"/data/LimeSurvey/lime_survey_dob_year.csv") ,row.names = FALSE)
  
  
  
  lime_your_role <- lime_survey[lime_survey$surey_question=='Which.of.the.following.best.describes.your.role.'  ,]
  lime_your_role <- setDT(lime_your_role)[,  list( value = (surey_answer[which.max(Date.submitted)]) )
                                          ,by='asic_id']
  
  
  lime_risk_advice <- lime_survey[lime_survey$surey_question=='Do.you.offer.risk.advice.'  ,]
  lime_risk_advice <- setDT(lime_risk_advice)[,  list( value = (surey_answer[which.max(Date.submitted)]) )
                                              ,by='asic_id']
  write.csv(lime_risk_advice,paste0(root_path,"/data/LimeSurvey/lime_survey_risk_advice.csv") )
  
  lime_how_many_advisers_in_practice <- lime_survey[lime_survey$surey_question=='How.many.advisers.are.there.in.your.practice.'  ,]
  lime_how_many_advisers_in_practice <- setDT(lime_how_many_advisers_in_practice)[,  list( value = (surey_answer[which.max(Date.submitted)]) )
                                                                                  ,by='asic_id']
  
  
  
  
  age_lime <- read.csv('age.csv')
  age_lime  <- age_lime [age_lime$Token%in% members$asic_id[members$member_status=='Current'],]
  
  age_lime  <- setDT(age_lime)[,list(age= round(mean(surey_answer)) ), by='Token']
  
  ages_spread <- data.frame(table(age_lime$age))
  ages_spread$Var1 <- as.numeric(as.character(ages_spread$Var1))
  
  ages_spread$perc <- round( ages_spread$Freq / sum(ages_spread$Freq)*100 , 2)
  
  
  
  sum(ages_spread$Freq)
  sum(ages_spread$Freq[ages_spread$Var1>=50])
  sum(ages_spread$perc[ages_spread$Var1>=50])
  
  
  
}

# typeform ----
if(FALSE)
{
  
  api  <- 'muRHnHDVECV8Yqn6EUyQ25qcJij8ne7wKxWELK7jRJa'
  

  # Was get_typeforms() in V1 of the package
  forms = get_forms(api)
  rtypeform_set_token(api)
  # form_id = forms$form_id[1]
  q = get_responses('OiILfNWM')
  
  q = get_responses('OiILfNWM', completed = TRUE,  page_size = 1000)
  

  questions <- fromJSON('https://api.typeform.com/forms/OiILfNWM')$fields
  
  questions <- questions[questions$title %in% c('What is the platform you use *most*?', 'What is *your favourite* platform to use?','What is *your least-favourite* platform to use?'),]
  
  responses_all <- data.frame()
  for(i in 1:nrow(questions))
  {
    response_temp <- data.frame(q[questions$id[i]],stringsAsFactors = F)
    colnames(response_temp) <- c('type','landing_id','value')
    responses_all <- rbind(responses_all,response_temp)
  }
  
  
  users <- data.frame(q$meta,stringsAsFactors = F)
  
  responses_all <- merge(responses_all,select(users,landing_id,token...11) , by='landing_id')
  
  write.csv(responses_all,paste0(root_path,"/data/TypeForm/responses_all.csv") )
  
}



lime_survey_dob_year <- read.csv(paste0(root_path,"/data/LimeSurvey/lime_survey_dob_year.csv"),stringsAsFactors = FALSE)
lime_survey_dob_year <- lime_survey_dob_year[lime_survey_dob_year$asic_id %in% members$asic_id,]

for(i in 1:nrow(lime_survey_dob_year))
{
  index <- which(members$asic_id == lime_survey_dob_year$asic_id[i])
  
  members$dob_year[index] <- lime_survey_dob_year$dob_year[i]
  members$dob_year_source[index] <- 'LimeSurvey'
  
}




lime_survey_risk_advice <- read.csv(paste0(root_path,"/data/LimeSurvey/lime_survey_risk_advice.csv"),stringsAsFactors = FALSE)
members$is_risk_adviser [members$asic_id %in% lime_survey_risk_advice$asic_id] <- 'TRUE'






#Licence ----
print("licence  ")

licence <- setDT(financial_planner)[,list( active = ifelse(sum(ADV_ROLE_STATUS=='Current')>0, TRUE,FALSE)
)
,by=c('LICENCE_NUMBER','LICENCE_NAME','LICENCE_ABN','LICENCE_CONTROLLED_BY')]


colnames(licence)    <- tolower(colnames(licence))
colnames(licence)[1] <- 'licence_id'







licence_gov <- read.csv(paste0(root_path,"/data/LICENCE/LICENCE_LATEST.csv"), stringsAsFactors = FALSE)

colnames(licence_gov)[colnames(licence_gov)=="AFS_LIC_NUM"] <- "licence_id"
colnames(licence_gov)[colnames(licence_gov)=="AFS_LIC_NAME"] <- "licence_name"
colnames(licence_gov)[colnames(licence_gov)=="AFS_LIC_START_DT"] <- "start_date"
colnames(licence_gov)[colnames(licence_gov)=="AFS_LIC_ADD_PCODE"] <- "address_postcode"
colnames(licence_gov)[colnames(licence_gov)=="AFS_LIC_ADD_STATE"] <- "address_state"
colnames(licence_gov)[colnames(licence_gov)=="AFS_LIC_ADD_LOCAL"] <- "address_city"
licence_gov <- select(licence_gov, licence_id,start_date,address_postcode, address_state, address_city )

licence <- merge(licence,licence_gov, by.x='licence_id', by.y= 'licence_id' ,all.x=TRUE)
licence[is.na(licence)] <- ""

#
Fortnum <- readxl::read_xlsx(paste0(root_path,"/data/FromAndrew/Fortnum AFSL Contact List. C Williams _ 13.01.2020.xlsx"))

Fortnum <- select(Fortnum,  licence_id= 'AFSL Short'
                  , key_person_name='Key Person'
                  , key_person_email='Contact Email Key Person'
                  , key_person_role= 'Role'
                  , key_person_phone= 'Contact Ph'
                  )
Fortnum$key_person_email[!grepl('@', Fortnum$key_person_email)] <- ''

licence <- merge(licence, Fortnum, by='licence_id',all.x=TRUE)
licence[is.na(licence)] <- ""

licence$parent_entity  <- ''
# add ownership
licence_ownership <- read.csv(paste0(root_path,"/data/LICENCE/200430 - Licensee ownership v7.csv"), stringsAsFactors = FALSE)
licence_ownership <- select(licence_ownership, parent_entity= Parent.Entity.Licensee.owner,licence_name = Licensee.name)
licence_ownership <- licence_ownership[licence_ownership$licence_name!='',]
licence_ownership$licence_name <- trimws(licence_ownership$licence_name)
licence_ownership$parent_entity <- trimws(licence_ownership$parent_entity)

for(i in 1:nrow(licence_ownership))
{ if(licence_ownership$parent_entity[i]=='')
{licence_ownership$parent_entity[i] <- licence_ownership$parent_entity[i-1]}
}

for(i in 1:nrow(licence_ownership))
{ 
  index= c()
  index = which( substring(licence$licence_name,1, nchar(licence_ownership$licence_name[i]))== licence_ownership$licence_name[i])
  if(length(index) >0 )
  {
    licence$parent_entity[index] <- licence_ownership$parent_entity[i]
  }
}







# DB_write_table(licence        ,"anubisda_advisers","licences")


# licence_stats <- Table_stats(licence,'licence')
# write.csv(licence_stats,'Docs/licence_stats.csv',row.names = FALSE)

# Convert to Json 
members$asic_member_status <- members$member_status
output_features <- c(
                     'year_covered_with_licence'
                     # ,'memberships'
                     # ,'qualifications'
                     ,'licence_id_previous'
                     ,'licence_name_previous'
                     ,'list_of_authorized_financial_services'
                     ,'number_of_authorized_financial_services'
                     ,'member_status')

members$json_asic <- Tools.Columns2Json(members,output_features)
members <- members[, !colnames(members) %in% output_features]

member_licences_unique <- setDT(member_licences)[,list(start_date=start_date[which.max(start_date)]
                                                      , end_date=end_date[which.max(start_date)])
                                                      ,by=c('asic_id','licence_id')]

member_licences_unique$start_date <- as.character(member_licences_unique$start_date)
member_licences_unique$end_date   <- as.character(member_licences_unique$end_date)


members <- merge(members,select(member_licences_unique,asic_id,licence_id,licence_start_date=start_date, licence_end_date=end_date), by=c('asic_id','licence_id'))

members_asic <- members


# remove ceased members that have same name with current member

members_asic <- members_asic[!(members_asic$asic_member_status=='Ceased'& members_asic$full_name %in% members$full_name[members$asic_member_status=='Current']) ,]
