
library(dplyr)
library(data.table)
library(rvest)
library(urltools)
library(tm)
library(parallel)
library(RMySQL)


root_path          <- paste0(here::here(),'/..')
source(paste0(root_path,'/R/Tools.R'), echo=TRUE)

root_path_linkedin <- paste0(root_path,'/data/GoogleSearch/search_by_name_company') 


#----  Get Linkedin url ----- 


Get_job_title_from_name_company <- function(i)
{
  print(i)
  library(RMySQL)
  library(rvest)
  # finalcial_planners <- DB_runQuery_retrurnDataFrame ( paste0("select row_names, ADV_NUMBER, first_name, last_name,LICENCE_NAME ,linkedin_url_tried, linkedin_url from advisor_express
  #                                                              where linkedin_url_tried ='' limit 1 offset ",i-1) )
  name <- paste0("site:au.linkedin.com/in ",list_name_company$full_name[i] , ',' , list_name_company$company[i] )
  # print(paste0("finding the url for:", name))
  url = URLencode(paste0("https://www.google.com/search?q=", name))
  
  ht <-  read_html(url) 
  
  result <- unique(ht %>% html_nodes(xpath='//a')%>%  html_text())
  result <- result[!grepl("site",tolower(result) )]
  result <- result[grepl("linkedin",tolower(result) )]
  result <- trimws( result[grepl(tolower(list_name_company$full_name[i] ),tolower(result) )])
  result <- trimws( result[grepl(substring(tolower(list_name_company$company[i] ),1,4)  ,tolower(result) )])
  
  
  if(length(result)==0 & list_name_company$name_is_unique[i])
  {
    result <- unique(ht %>% html_nodes(xpath='//a')%>%  html_text())
    result <- result[!grepl("site",tolower(result) )]
    result <- result[grepl("linkedin",tolower(result) )]
    result <- trimws( result[grepl(tolower(list_name_company$full_name[i] ),tolower(result) )])
  }
  
  if(length(result)>0)
  {
    list_name_company$linkedin_result[i]= result[1]
  }
  write.csv(list_name_company[i,],paste0(root_path_linkedin,"/",list_name_company$full_name[i] , ',' , list_name_company$company[i] ,".csv"), row.names = FALSE)
  
}



list_name_company <- read.csv(paste0(root_path_linkedin,"/../search_by_name_company_input.csv"),stringsAsFactors = FALSE)
list_name_company$linkedin_result <- ''

list_name_company <- setDT(list_name_company)[,name_is_unique := .N==1,by='full_name']




list_files <- list.files(paste0(root_path_linkedin))

list_name_company <- list_name_company[!paste0(list_name_company$full_name , ',' , list_name_company$company,'.csv') %in%list_files,  ]



clust <- makeCluster(30)
clusterExport(cl=clust, varlist=c("list_name_company",'root_path_linkedin'))
system.time({a <- parLapply(clust, 1:nrow(list_name_company), Get_job_title_from_name_company)})
stopCluster(clust)



#Merge all and clean ---- 


read_csv <- function(i)
{
  out <- tryCatch(
    {
      read.csv(paste0(root_path_linkedin,"/", files[i] ), stringsAsFactors = F)
    },
    error=function(cond) {
    }
  )
  return( out)
}

files <- list.files(paste0(root_path_linkedin))
files <- files[grepl('.csv',files)]



clust <- makeCluster(5)
clusterExport(cl=clust, varlist=c("files","root_path_linkedin"))
system.time({ a <- parLapply(clust, 1:length(files), read_csv)})
stopCluster(clust)


job_titles <- rbindlist(a)
job_titles <- job_titles[!is.na(job_titles$linkedin_result),]


job_titles$linkedin_url <- job_titles$linkedin_result


job_titles <- cbind(job_titles,First_last_from_full_name(job_titles$full_name))
job_titles$company_input <- job_titles$company  


job_titles$linkedin_url <- gsub('‹','<', job_titles$linkedin_url)
job_titles$linkedin_url <- gsub('›','>', job_titles$linkedin_url)

job_titles <- LinkedIn_clean(job_titles)

job_titles$company <- trimws(gsub('https://','',job_titles$company))


job_titles <- select(job_titles,full_name, company=company_input,linkedin_url=linkedin_url_clean,job_title)

write.csv(job_titles,paste0(root_path_linkedin,'/../search_by_name_company.csv') ,row.names = FALSE)



